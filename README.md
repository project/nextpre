# Next Previous Post Block

  Offering NEXT PREVIOUS POST BLOCK without going to the listing page.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/nextpre).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/nextpre).

## Table of contents

- Installation
- Requirements
- Configuration
- Uninstallation
- UI Information

## Installation

- Install as usual, see [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-modules)
  for further information.

## Requirements
  The basic nextpre module has node dependencies, nothing special is required.

## Configuration

  1. Install module “Next and previous link”.
  2. Go to the “Block Layout”. Eg:- Admin Menu >> structure >> block layout
  3. Go to the your block region.
  4. Click the "Place block" button and in the modal dialog click the 
     "Place block" button next to "Next Previous link".
  5. On the block configuration form you can choose the node bundle name to 
     filter and the next/previous labels the buttons will have.

## Uninstallation

  1. Uninstall as you would normally Uninstall a contributed Drupal module. 
     See: [Uninstall](https://www.drupal.org/docs/user_guide/en/config-uninstall.html)
     for further information.

## UI Information

  1. For the UI, the Developer will add the class in both buttons 
     during block configuration.
